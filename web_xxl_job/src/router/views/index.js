import Layout from '@/page/index/'
import Store from '@/store/'

export default [
    {
        path: '/wel',
        component: () => Store.getters.isMacOs ? import('@/mac/index.vue') : import('@/page/index/index.vue'),
        redirect: '/wel/index',
        children: [{
            path: 'index',
            name: '首页',
            meta: {
                i18n: 'dashboard'
            },
            component: () =>
                import( /* webpackChunkName: "views" */ '@/views/wel/index')
        }]
    }, {
        path: '/info',
        component: Layout,
        redirect: '/info/index',
        children: [{
            path: 'index',
            name: '修改密码',
            meta: {
                i18n: 'info'
            },
            component: () =>
                import( /* webpackChunkName: "views" */ '@/views/user/info')
        }]
    }
]