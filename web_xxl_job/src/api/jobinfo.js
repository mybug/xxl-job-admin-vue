import request from '@/router/axios';
import {baseUrl} from '@/config/env';

/**
 *
 * @param jobGroup
 * @returns {*}
 */
export const jobinfoDicData = (jobGroup) => request({
    url: baseUrl + '/api/xxl-job-admin/jobinfo/dicData',
    method: 'get',
    params: {
        jobGroup
    }
});

/**
 * jobGroup:1
 * triggerStatus:-1
 * jobDesc:
 * executorHandler:
 * author:
 * start:
 * length:
 * @param formData
 * @returns {*}
 */
export const jobinfoList = (formData) => request({
    url: baseUrl + '/api/xxl-job-admin/jobinfo/pageList',
    method: 'get',
    params: formData
});

/**
 * jobinfo:1
 * triggerStatus:-1
 * jobDesc
 * executorHandler
 * author
 * start:0
 * length:10
 * @param formData
 * @returns {*}
 */
export const jobinfoSave = (formData) => request({
    url: baseUrl + '/api/xxl-job-admin/jobinfo/add',
    method: 'get',
    params: formData
});

/**
 * id
 * jobinfo:1
 * triggerStatus:-1
 * jobDesc
 * executorHandler
 * author
 * start:0
 * length:10
 * @param formData
 * @returns {*}
 */
export const jobinfoUpdate = (formData) => request({
    url: baseUrl + '/api/xxl-job-admin/jobinfo/update',
    method: 'get',
    params: formData
});

/**
 *
 * @param id
 * @returns {*}
 */
export const jobinfoRemove = (id) => request({
    url: baseUrl + '/api/xxl-job-admin/jobinfo/remove',
    method: 'get',
    params: {
        id
    }
});

/**
 *
 * @param id
 * @returns {*}
 */
export const jobinfoStop = (id) => request({
    url: baseUrl + '/api/xxl-job-admin/jobinfo/stop',
    method: 'get',
    params: {
        id
    }
});

/**
 *
 * @param id
 * @returns {*}
 */
export const jobinfoStart = (id) => request({
    url: baseUrl + '/api/xxl-job-admin/jobinfo/start',
    method: 'get',
    params: {
        id
    }
});

/**
 *
 * @param id
 * @param executorParam
 * @param addressList
 * @returns {AxiosPromise}
 */
export const jobinfoTrigger = (id, executorParam, addressList) => request({
    url: baseUrl + '/api/xxl-job-admin/jobinfo/trigger',
    method: 'get',
    params: {
        id, executorParam, addressList
    }
});

/**
 *
 * @param scheduleType
 * @param scheduleConf
 * @returns {AxiosPromise}
 */
export const jobinfoNextTriggerTime = (scheduleType, scheduleConf) => request({
    url: baseUrl + '/api/xxl-job-admin/jobinfo/nextTriggerTime',
    method: 'get',
    params: {
        scheduleType, scheduleConf
    }
});


