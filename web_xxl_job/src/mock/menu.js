import Mock from 'mockjs'

function randomColor() {
    return '#' + Math.random().toString(16).substr(2, 6).toUpperCase();
}

const top = [
    {
        label: "首页",
        path: "/wel/index",
        icon: 'el-icon-document',
        meta: {
            i18n: 'dashboard',
        },
        parentId: 0
    }
]
const first = [
    {
        label: "首页",
        path: "/wel/index",
        icon: 'el-icon-document',
        meta: {
            i18n: 'dashboard',
        },
        parentId: 0
    },
    {
        label: "任务管理",
        path: '/jobinfo',
        component: 'views/jobinfo/index',
        icon: 'icon-caidan',
        iconBgColor: randomColor(),
        children: []
    },
    {
        label: "调度日志",
        path: '/joblog',
        component: 'views/joblog/index',
        icon: 'icon-caidan',
        iconBgColor: randomColor(),
        children: []
    },
    {
        label: "执行器管理",
        path: '/jobgroup',
        component: 'views/jobgroup/index',
        icon: 'icon-caidan',
        iconBgColor: randomColor(),
        children: []
    },
    {
        label: "用户管理",
        path: '/user',
        component: 'views/user/index',
        icon: 'icon-caidan',
        iconBgColor: randomColor(),
        children: []
    },
    {
        label: "使用教程",
        path: '/help',
        component: 'views/util/help',
        icon: 'icon-caidan',
        iconBgColor: randomColor(),
        children: []
    }
    // {
    //     label: '多级菜单',
    //     path: '/deep',
    //     iconBgColor: randomColor(),
    //     children: [{
    //         label: '多级菜单1-1',
    //         path: 'deep',
    //         iconBgColor: randomColor(),
    //         children: [{
    //             label: '多级菜单2-1',
    //             path: 'deep',
    //             iconBgColor: randomColor(),
    //             component: 'views/util/deep',
    //         }]
    //     }]
    // },
    // {
    //     label: "外部页面",
    //     path: '/out',
    //     icon: 'icon-caidan',
    //     iconBgColor: randomColor(),
    //     meta: {
    //         i18n: 'out',
    //     },
    //     children: [{
    //         label: "官方网站(内嵌页面)",
    //         path: 'website',
    //         href: 'https://avuejs.com',
    //         icon: 'icon-caidan',
    //         iconBgColor: randomColor(),
    //         meta: {
    //             i18n: 'website',
    //         }
    //     }, {
    //         label: "全局函数(外链页面)",
    //         path: 'api',
    //         href: 'https://avuejs.com/docs/api?test1=1&test2=2',
    //         icon: 'icon-caidan',
    //         iconBgColor: randomColor(),
    //         meta: {
    //             target: '_blank',
    //             i18n: 'api',
    //         }
    //     }]
    // },
    // {
    //     label: "异常页",
    //     path: '/error',
    //     meta: {
    //         i18n: 'error',
    //     },
    //     icon: 'icon-caidan',
    //     iconBgColor: randomColor(),
    //     children: [{
    //         label: "error403",
    //         path: 'error',
    //         component: 'components/error-page/403',
    //         icon: 'icon-caidan',
    //         iconBgColor: randomColor(),
    //         children: []
    //     }, {
    //         label: "error404",
    //         path: '404',
    //         component: 'components/error-page/404',
    //         icon: 'icon-caidan',
    //         iconBgColor: randomColor(),
    //         children: []
    //     }, {
    //         label: "error500",
    //         path: '500',
    //         component: 'components/error-page/500',
    //         icon: 'icon-caidan',
    //         iconBgColor: randomColor(),
    //         children: []
    //     }]
    // },
    // {
    //     label: "使用教程",
    //     path: '/help',
    //     component: 'views/util/help',
    //     icon: 'icon-caidan',
    //     iconBgColor: randomColor(),
    //     children: []
    // }
]
const second = [
]
export default ({mock}) => {
    if (!mock) return;
    let menu = [first, second];
    console.log(menu)
    Mock.mock('/user/getMenu', 'get', (res) => {
        let body = JSON.parse(res.body);
        return {
            data: menu[body.type] || []
        }
    })
    Mock.mock('/user/getTopMenu', 'get', () => {
        return {
            data: top
        }
    })

}