# xxl-job-admin-vue 前后端分离项目

## 介绍

基于avue-cli、avue和element-ui实现的开源免费的vue版本xxl-job管理端

前端基于avue-cli 2.x分支
服务端基于xxl-job 2.3.1分支

暂未兼容其他版本

## 对接进度

### 已完成对接功能

1、用户登录、修改密码

2、用户管理

3、执行器管理

4、任务管理

5、调度日志

### 对接计划

1、GLUE IDE编辑器

2、执行日志 Console

3、支持集成到xxl-job-admin

## 服务端兼容性优化

```java
//IndexController line 47
/**
 * 添加首页卡片统计数据接口
 * @return
 */
@RequestMapping("/dashboardInfo")
@ResponseBody
public ReturnT<Map<String, Object>>dashboardInfo(){
    Map<String, Object> dashboardInfo=xxlJobService.dashboardInfo();
    return new ReturnT<>(dashboardInfo);
}
```

```java
//JobInfoController line 74
@RequestMapping("/dicData")
@ResponseBody
public Map<String,Object> dicData(HttpServletRequest request, @RequestParam(required = false, defaultValue = "-1") int jobGroup) {
    Map<String,Object> map = new LinkedHashMap<>();
    // 枚举-字典
    List<Map<String,Object>> RouteStrategyList = new ArrayList<>();
    ExecutorRouteStrategyEnum[] enums1 = ExecutorRouteStrategyEnum.values();
    for (ExecutorRouteStrategyEnum anEnum : enums1) {
        Map<String,Object> hashMap = new LinkedHashMap<>();
        hashMap.put("label",anEnum.getTitle());
        hashMap.put("value",anEnum.name());
        RouteStrategyList.add(hashMap);
    }
    map.put("ExecutorRouteStrategyEnum", RouteStrategyList);	    // 路由策略-列表

    List<Map<String,Object>> GlueTypeList = new ArrayList<>();
    GlueTypeEnum[] enums2 = GlueTypeEnum.values();
    for (GlueTypeEnum anEnum : enums2) {
        Map<String,Object> hashMap = new LinkedHashMap<>();
        hashMap.put("label",anEnum.getDesc());
        hashMap.put("value",anEnum.name());
        GlueTypeList.add(hashMap);
    }
    map.put("GlueTypeEnum", GlueTypeList);								// Glue类型-字典

    List<Map<String,Object>> BlockStrategyList = new ArrayList<>();
    ExecutorBlockStrategyEnum[] enums3 = ExecutorBlockStrategyEnum.values();
    for (ExecutorBlockStrategyEnum anEnum : enums3) {
        Map<String,Object> hashMap = new LinkedHashMap<>();
        hashMap.put("label",anEnum.getTitle());
        hashMap.put("value",anEnum.name());
        BlockStrategyList.add(hashMap);
    }
    map.put("ExecutorBlockStrategyEnum", BlockStrategyList);	    // 阻塞处理策略-字典

    List<Map<String,Object>> ScheduleTypeList = new ArrayList<>();
    ScheduleTypeEnum[] enums = ScheduleTypeEnum.values();
    for (ScheduleTypeEnum anEnum : enums) {
        Map<String,Object> hashMap = new LinkedHashMap<>();
        hashMap.put("label",anEnum.getTitle());
        hashMap.put("value",anEnum.name());
        ScheduleTypeList.add(hashMap);
    }
    map.put("ScheduleTypeEnum", ScheduleTypeList);	    				// 调度类型

    List<Map<String,Object>> MisfireStrategyList = new ArrayList<>();
    MisfireStrategyEnum[] enums4 = MisfireStrategyEnum.values();
    for (MisfireStrategyEnum anEnum : enums4) {
        Map<String,Object> hashMap = new LinkedHashMap<>();
        hashMap.put("label",anEnum.getTitle());
        hashMap.put("value",anEnum.name());
        MisfireStrategyList.add(hashMap);
    }
    map.put("MisfireStrategyEnum", MisfireStrategyList);	    			// 调度过期策略

    // 执行器列表
    List<XxlJobGroup> jobGroupList_all =  xxlJobGroupDao.findAll();

    // filter group
    List<XxlJobGroup> jobGroupList = filterJobGroupByRole(request, jobGroupList_all);
    if (jobGroupList==null || jobGroupList.size()==0) {
        throw new XxlJobException(I18nUtil.getString("jobgroup_empty"));
    }

    map.put("JobGroupList", jobGroupList);
    map.put("jobGroup", jobGroup);

    return map;
}
```

```java
// UserController line 36

public static final String LOGIN_IDENTITY_KEY = "XXL_JOB_LOGIN_IDENTITY";

@Resource
private LoginService loginService;

/**
 * 登录状态检测
 * @param request
 * @param response
 * @return
 */
@RequestMapping("/refesh")
@ResponseBody
@PermissionLimit(limit=false)
public ReturnT<String> refesh(HttpServletRequest request, HttpServletResponse response) {
    if (loginService.ifLogin(request, response) != null) {
        String loginToken = CookieUtil.getValue(request, LOGIN_IDENTITY_KEY);
        return new ReturnT<>(loginToken);
    }
    return ReturnT.FAIL;
}

@RequestMapping("/groupList")
@ResponseBody
public List<XxlJobGroup> groupList() {
    // 执行器列表
    List<XxlJobGroup> groupList = xxlJobGroupDao.findAll();
    return groupList;
}
```

```java
// PermissionInterceptor line 45
String xHeader = request.getHeader("X-Requested-With");
if (Objects.equals(xHeader,"XMLHttpRequest")){
	//支持异接口拦截响应，ajax请求返回401状态码
	response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
	return false;
}
```

```java
//LoginService line 65
return new ReturnT<>(loginToken);
```
## 系统截图

登录页面
![img.png](docs/images/img.png)

首页
![img_1.png](docs/images/img_1.png)

任务管理
![img_2.png](docs/images/img_2.png)

调度日志
![img_3.png](docs/images/img_3.png)

执行器管理
![img_4.png](docs/images/img_4.png)

用户管理
![img_5.png](docs/images/img_5.png)

使用教程
![img_6.png](docs/images/img_6.png)

修改密码
![img_7](docs/images/img_7.png)

## 关于我

<div style="display: flex;justify-content: center">
<img src="docs/images/2.png" width="220" >
</div>